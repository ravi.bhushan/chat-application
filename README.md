# nodejs-reactjs-chatapp

Create chat application using Nodejs Expressjs, mongodb, Reactjs.


## Server

``` 
cd server 
```
```
npm install
```

```
npm run dev
```
### Reactjs App development

```
cd client
```

```
npm start
```

### To run mongod on server

```
cd server 

```

```
mongod

```


