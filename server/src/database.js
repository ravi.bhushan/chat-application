const {MongoClient} = require('mongodb');

const URL = 'mongodb://localhost:27017/chatapp';

// const client = new MongoClient(URL);
export default class Database{

	connect(){


		return new Promise((resolve, reject) => {

			MongoClient.connect(URL, (err, db) => {
				
				return err ? reject(err) : resolve(db);

			});


		});
	}
}
